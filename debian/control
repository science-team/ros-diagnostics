Source: ros-diagnostics
Section: libs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>,
           Johannes 'josch' Schauer <josch@debian.org>
Build-Depends: debhelper-compat (= 13), dh-ros (>= 0.13.4), catkin, python3-all, python3-setuptools, dh-sequence-python3, libdiagnostic-msgs-dev, pluginlib-dev, libroscpp-dev, python3-rospy, librostest-dev, librosbag-dev, libbondcpp-dev, libgtest-dev, python3-rostest <!nocheck>, python3-diagnostic-msgs <!nocheck>, python3-psutil <!nocheck>, python3-rosbag <!nocheck>, python3-bondpy <!nocheck>
Standards-Version: 4.7.1
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/diagnostics
Vcs-Browser: https://salsa.debian.org/science-team/ros-diagnostics
Vcs-Git: https://salsa.debian.org/science-team/ros-diagnostics.git

Package: libdiagnostic-aggregator-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libdiagnostic-aggregator1d (= ${binary:Version}), ${misc:Depends}, libdiagnostic-msgs-dev, pluginlib-dev, libroscpp-dev, python3-rospy, librostest-dev, libxmlrpcpp-dev
Description: development files for diagnostic_aggregator (Robot OS)
 The diagnostic_aggregator contains a ROS node, aggregator_node, that listens
 to diagnostic_msgs/DiagnosticArray messages on the /diagnostics topic,
 processes and categorizes the data, and republishes on /diagnostics_agg. The
 aggregator_node loads "Analyzer" plugins to perform the diagnostics processing
 and categorization. The configuration and setup for each diagnostic aggregator
 is specific to each robot and can be determined by users or developers.
 .
 This package contains the development files for the library.

Package: libdiagnostic-aggregator1d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: library for diagnostic_aggregator (Robot OS)
 The diagnostic_aggregator contains a ROS node, aggregator_node, that listens
 to diagnostic_msgs/DiagnosticArray messages on the /diagnostics topic,
 processes and categorizes the data, and republishes on /diagnostics_agg. The
 aggregator_node loads "Analyzer" plugins to perform the diagnostics processing
 and categorization. The configuration and setup for each diagnostic aggregator
 is specific to each robot and can be determined by users or developers.
 .
 This package contains the C library.

Package: libdiagnostic-aggregator-tools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, python3
Multi-Arch: foreign
Description: Robot OS diagnostic_aggregator tools
 The diagnostic_aggregator contains a ROS node, aggregator_node, that listens
 to diagnostic_msgs/DiagnosticArray messages on the /diagnostics topic,
 processes and categorizes the data, and republishes on /diagnostics_agg. The
 aggregator_node loads "Analyzer" plugins to perform the diagnostics processing
 and categorization. The configuration and setup for each diagnostic aggregator
 is specific to each robot and can be determined by users or developers.
 .
 This package contains the tools.

Package: python3-diagnostic-analysis
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python library diagnostic_analysis (Robot OS)
 This package is part of Robot OS (ROS). The diagnostic_analysis package can
 convert a log of diagnostics data into a series of CSV files. Robot logs are
 recorded with rosbag, and can be processed offline using the scripts in this
 package.

Package: libdiagnostic-updater-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libdiagnostic-updater0d (= ${binary:Version}), libdiagnostic-msgs-dev, libroscpp-dev, librostest-dev
Description: development files for diagnostic_updater (Robot OS)
 This package is part of Robot OS (ROS). diagnostic_updater contains tools for
 easily updating diagnostics. it is commonly used in device drivers to keep
 track of the status of output topics, device status, etc.
 .
 This package contains the development files for the library.

Package: libdiagnostic-updater0d
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library for diagnostic_updater (Robot OS)
 This package is part of Robot OS (ROS). diagnostic_updater contains tools for
 easily updating diagnostics. it is commonly used in device drivers to keep
 track of the status of output topics, device status, etc.
 .
 This package contains the shared library.

Package: python3-diagnostic-updater
Section: python
Architecture: all
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}, python3-rospy, python3-diagnostic-msgs
Description: Python library diagnostic_updater (Robot OS)
 This package is part of Robot OS (ROS). The diagnostic_updater package can
 convert a log of diagnostics data into a series of CSV files. Robot logs are
 recorded with rosbag, and can be processed offline using the scripts in this
 package.

Package: python3-diagnostic-common-diagnostics
Section: python
Architecture: any
Multi-Arch: allowed
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python library diagnostic_common_diagnostics (Robot OS)
 This package is part of Robot OS (ROS). The diagnostic_common_diagnostics
 package can convert a log of diagnostics data into a series of CSV files.
 Robot logs are recorded with rosbag, and can be processed offline using the
 scripts in this package.

Package: libself-test-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libdiagnostic-msgs-dev, libdiagnostic-updater-dev, libroscpp-dev, librostest-dev
Description: development files for self_test (Robot OS)
 This package is part of Robot OS (ROS). It uses the diagnostic_updater to
 perform a self test on a driver, using a special service call.
 .
 This package contains the development files for the library.

Package: libself-test-tools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, python3
Multi-Arch: foreign
Description: Robot OS self_test tools
 This package is part of Robot OS (ROS). It uses the diagnostic_updater to
 perform a self test on a driver, using a special service call.
 .
 This package contains the tools.

Package: rosdiagnostic
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: command line tool to print aggregated diagnostic (Robot OS)
 This package is part of Robot OS (ROS). It contains a command to print
 aggregated diagnostic contents to the command line.
